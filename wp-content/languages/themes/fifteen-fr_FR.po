# Translation of Fifteen in French (France)
# This file is distributed under the same license as the Fifteen package.
msgid ""
msgstr ""
"PO-Revision-Date: 2015-07-18 23:43:14+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: GlotPress/1.0-alpha-1100\n"
"Project-Id-Version: Fifteen\n"

#: sidebar.php:24
msgid "Meta"
msgstr "Méta"

#: searchform-top.php:15
msgid "Cancel"
msgstr "Annuler"

#: searchform-top.php:11 searchform.php:11
msgctxt "label"
msgid "Search for:"
msgstr "Rechercher&nbsp;:"

#: options.php:211
msgid "Check this if you do not want to give us credit in your site footer."
msgstr "Check this if you do not want to give us credit in your site footer."

#: options.php:202
msgid "To Regenerate all Thumbnail images, Install and Activate the <a href=\"http://wordpress.org/plugins/regenerate-thumbnails/\" target=\"_blank\">Regenerate Thumbnails</a> WP Plugin. Then from <strong>Tools &gt; Regen. Thumbnails</strong>, re-create thumbnails for all your existing images. And your blog will look even more stylish with Fifteen theme.<br /> "
msgstr "To Regenerate all Thumbnail images, Install and Activate the <a href=\"http://wordpress.org/plugins/regenerate-thumbnails/\" target=\"_blank\">Regenerate Thumbnails</a> WP Plugin. Then from <strong>Tools &gt; Regen. Thumbnails</strong>, re-create thumbnails for all your existing images. And your blog will look even more stylish with Fifteen theme.<br /> "

#: options.php:198
msgid "If you are using Fifteen Theme on a New Wordpress Installation, then you can skip this section.<br />But if you have just switched to this theme from some other theme, then you are requested regenerate all the post thumbnails. It will fix all the issues you are facing with distorted & ugly homepage thumbnail Images. "
msgstr "If you are using Fifteen Theme on a New Wordpress Installation, then you can skip this section.<br />But if you have just switched to this theme from some other theme, then you are requested regenerate all the post thumbnails. It will fix all the issues you are facing with distorted & ugly homepage thumbnail Images. "

#: options.php:206
msgid "<strong>Note:</strong> Regenerating the thumbnails, will not affect your original images. It will just generate a separate image file for those images."
msgstr "<strong>Note:</strong> Regenerating the thumbnails, will not affect your original images. It will just generate a separate image file for those images."

#: options.php:210
msgid "Theme Credits"
msgstr "Theme Credits"

#: search.php:10
msgid "Search Results for: %s"
msgstr "Résultats de recherche pour %s"

#: options.php:193
msgid "For your convenience, we have created a <a href=\"http://demo.inkhive.com/fifteen/\" target=\"_blank\">Live Demo Blog of Fifteen</a>. You can take a look at and find out how your site would look once complete."
msgstr "For your convenience, we have created a <a href=\"http://demo.inkhive.com/fifteen/\" target=\"_blank\">Live Demo Blog of Fifteen</a>. You can take a look at and find out how your site would look once complete."

#: options.php:192
msgid "Live Demo Blog"
msgstr "Live Demo Blog"

#: options.php:188
msgid "We offer Dedicated and Fast e-mail Support only for Pro Version Customers. <a href=\"http://inkhive.com/product/fifteen-plus/\" target=\"_blank\">Upgrade to Pro at $24.90</a>."
msgstr "We offer Dedicated and Fast e-mail Support only for Pro Version Customers. <a href=\"http://inkhive.com/product/fifteen-plus/\" target=\"_blank\">Upgrade to Pro at $24.90</a>."

#: options.php:187
msgid "Dedicated Support"
msgstr "Dedicated Support"

#: options.php:179
msgid "Fifteen WordPress theme has been Designed and Created by <a href=\"http://InkHive.com\" target=\"_blank\">InkHive</a>. For any Queries or help regarding this theme, <a href=\"http://wordpress.org/support/theme/fifteen/\" target=\"_blank\">use the WordPress support forums</a>."
msgstr "Fifteen WordPress theme has been Designed and Created by <a href=\"http://InkHive.com\" target=\"_blank\">InkHive</a>. For any Queries or help regarding this theme, <a href=\"http://wordpress.org/support/theme/fifteen/\" target=\"_blank\">use the WordPress support forums</a>."

#: options.php:183
msgid "<a href=\"http://twitter.com/rohitinked\" target=\"_blank\">Follow Me on Twitter</a> to know about my upcoming themes."
msgstr "<a href=\"http://twitter.com/rohitinked\" target=\"_blank\">Follow Me on Twitter</a> to know about my upcoming themes."

#: options.php:197
msgid "Regenerating Post Thumbnails"
msgstr "Regenerating Post Thumbnails"

#: options.php:171
msgid "For more Icons, Custom Icons and Choice of images - <a href=\"http://inkhive.com/product/fifteen-plus/\" target=\"_blank\">Upgrade to Pro at $24.90</a>."
msgstr "For more Icons, Custom Icons and Choice of images - <a href=\"http://inkhive.com/product/fifteen-plus/\" target=\"_blank\">Upgrade to Pro at $24.90</a>."

#: options.php:170
msgid "More Icons & Images"
msgstr "More Icons & Images"

#: options.php:163
msgid "Your Flickr Profile URL"
msgstr "Your Flickr Profile URL"

#: options.php:175
msgid "Support"
msgstr "Support"

#: options.php:155
msgid "Your Youtube Channel URL"
msgstr "Your Youtube Channel URL"

#: options.php:162
msgid "Flickr"
msgstr "Flickr"

#: options.php:154
msgid "Youtube"
msgstr "YouTube"

#: options.php:146
msgid "Linked In"
msgstr "Linked In"

#: options.php:147
msgid "Your Linked In Profile URL"
msgstr "Your Linked In Profile URL"

#: options.php:139
msgid "Your Instagram Profile URL"
msgstr "Your Instagram Profile URL"

#: options.php:138
msgid "Instagram"
msgstr "Instagram"

#: options.php:131
msgid "Your Pinterest Profile URL"
msgstr "Your Pinterest Profile URL"

#: options.php:130
msgid "Pinterest"
msgstr "Pinterest"

#: options.php:123
msgid "URL for your RSS Feeds"
msgstr "URL for your RSS Feeds"

#: options.php:122
msgid "Feeburner"
msgstr "Feeburner"

#: options.php:115
msgid "Google Plus profile url, including \"http://\""
msgstr "Google Plus profile url, including \"http://\""

#: options.php:114
msgid "Google Plus"
msgstr "Google Plus"

#: options.php:107
msgid "Twitter Username"
msgstr "Twitter Username"

#: options.php:106
msgid "Twitter"
msgstr "Twitter"

#: options.php:99
msgid "Facebook Profile or Page URL i.e. http://facebook.com/username/ "
msgstr "Facebook Profile or Page URL i.e. http://facebook.com/username/ "

#: options.php:88
msgid "Fifteen Plus Supports a Featured Slider with over 16 Animation Effects and plenty of other configuration options. <a href=\"http://inkhive.com/product/fifteen-plus/\" target=\"_blank\">Upgrade to Pro at $24.90</a>."
msgstr "Fifteen Plus Supports a Featured Slider with over 16 Animation Effects and plenty of other configuration options. <a href=\"http://inkhive.com/product/fifteen-plus/\" target=\"_blank\">Upgrade to Pro at $24.90</a>."

#: options.php:87
msgid "More Layout Options"
msgstr "More Layout Options"

#: options.php:98
msgid "Facebook"
msgstr "Facebook"

#: options.php:94
msgid "Social Settings"
msgstr "Social Settings"

#: options.php:81
msgid "Some Custom Styling for your site. Place any css codes here instead of the style.css file."
msgstr "Some Custom Styling for your site. Place any css codes here instead of the style.css file."

#: options.php:80
msgid "Custom CSS"
msgstr "CSS personnalisé"

#: options.php:59
msgid "More Settings Like Analytics, Footer Codes, Header Codes, Responsive Navigation, etc are available in Fifteen plus. <a href=\"http://inkhive.com/product/fifteen-plus/\" target=\"_blank\">Upgrade to Pro at $24.90</a>."
msgstr "More Settings Like Analytics, Footer Codes, Header Codes, Responsive Navigation, etc are available in Fifteen plus. <a href=\"http://inkhive.com/product/fifteen-plus/\" target=\"_blank\">Upgrade to Pro at $24.90</a>."

#: options.php:58
msgid "More Settings"
msgstr "More Settings"

#: options.php:65
msgid "Layout Settings"
msgstr "Layout Settings"

#: options.php:52
msgid "Some Text regarding copyright of your site, you would like to display in the footer."
msgstr "Some Text regarding copyright of your site, you would like to display in the footer."

#: options.php:51
msgid "Copyright Text"
msgstr "Texte de Copyright"

#: options.php:45
msgid "Leave Blank to use text Heading."
msgstr "Leave Blank to use text Heading."

#: options.php:44
msgid "Site Logo"
msgstr "Site Logo"

#: options.php:34
msgid "false"
msgstr "false"

#: options.php:33
msgid "true"
msgstr "true"

#: options.php:40
msgid "Basic Settings"
msgstr "Réglages de base"

#: no-results.php:28
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr "Il semblerait que nous ne soyons pas en mesure de trouver votre contenu. Essayez en lançant une recherche."

#: no-results.php:23
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr "Désolé, mais rien ne correspond à votre recherche. Veuillez réessayer avec des mots différents."

#: no-results.php:19
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr "Prêt à publier votre premier article&nbsp;? <a href=\"%1$s\">Lancez-vous</a>&nbsp;!"

#: no-results.php:13
msgid "Nothing Found"
msgstr "Rien de trouvé"

#: inc/widgets.php:97
msgid "No. of Posts:"
msgstr "No. of Posts:"

#: inc/widgets.php:13
msgid "Fifteen Recent Posts"
msgstr "Fifteen Recent Posts"

#: inc/template-tags.php:183
msgid "<span class=\"posted-on\"><i class=\"fa fa-clock-o\"> </i> %1$s</span> <span class=\"byline\"> <i class=\"fa fa-user\"> </i> %2$s</span>"
msgstr "<span class=\"posted-on\"><i class=\"fa fa-clock-o\"> </i> %1$s</span> <span class=\"byline\"> <i class=\"fa fa-user\"> </i> %2$s</span>"

#: inc/widgets.php:94
msgid "Title:"
msgstr "Titre&nbsp;:"

#: inc/widgets.php:90
msgid "5"
msgstr "5"

#: inc/widgets.php:84
msgid "Latest Articles"
msgstr "Latest Articles"

#: inc/widgets.php:14
msgid "Display your recent posts, with a Thumbnail."
msgstr "Display your recent posts, with a Thumbnail."

#: inc/template-tags.php:93
msgid "Your comment is awaiting moderation."
msgstr "Votre commentaire est en attente de validation."

#: inc/template-tags.php:71
msgid "Pingback:"
msgstr "Ping&nbsp;:"

#: inc/template-tags.php:48
msgid "Newer posts <span class=\"meta-nav\">&rarr;</span>"
msgstr "Articles plus récents <span class=\"meta-nav\">&rarr;</span>"

#: inc/template-tags.php:44
msgid "<span class=\"meta-nav\">&larr;</span> Older posts"
msgstr "<span class=\"meta-nav\">&larr;</span> Articles plus anciens"

#: inc/template-tags.php:39
msgctxt "Next post link"
msgid "&rarr;"
msgstr "&rarr;"

#: inc/template-tags.php:38
msgctxt "Previous post link"
msgid "&larr;"
msgstr "&larr;"

#: inc/template-tags.php:34
msgid "Post navigation"
msgstr "Navigation des articles"

#: inc/options-sanitize.php:354
msgid "Bold Italic"
msgstr "Italique gras"

#: inc/options-sanitize.php:353
msgid "Bold"
msgstr "Gras"

#: inc/options-sanitize.php:352
msgid "Italic"
msgstr "Italique"

#: inc/options-sanitize.php:351
msgid "Normal"
msgstr "Normal"

#: inc/options-sanitize.php:277
msgid "Fixed in Place"
msgstr "Fixe"

#: inc/options-sanitize.php:276
msgid "Scroll Normally"
msgstr "Défilement normal"

#: inc/options-sanitize.php:263
msgid "Bottom Right"
msgstr "Bas droite"

#: inc/options-sanitize.php:262
msgid "Bottom Center"
msgstr "En bas au centre"

#: inc/options-sanitize.php:261
msgid "Bottom Left"
msgstr "Bas gauche"

#: inc/options-sanitize.php:260
msgid "Middle Right"
msgstr "Au milieu à droite"

#: inc/options-sanitize.php:259
msgid "Middle Center"
msgstr "Au milieu à centre"

#: inc/options-sanitize.php:258
msgid "Middle Left"
msgstr "Au milieu à gauche"

#: inc/options-sanitize.php:257
msgid "Top Right"
msgstr "En haut à droite"

#: inc/options-sanitize.php:256
msgid "Top Center"
msgstr "En haut au centre"

#: inc/options-sanitize.php:255
msgid "Top Left"
msgstr "Haut gauche"

#: inc/options-sanitize.php:242
msgid "Repeat All"
msgstr "Répéter dans toutes les directions"

#: inc/options-sanitize.php:241
msgid "Repeat Vertically"
msgstr "Répéter verticalement"

#: inc/options-sanitize.php:240
msgid "Repeat Horizontally"
msgstr "Répéter horizontalement"

#: inc/options-sanitize.php:239
msgid "No Repeat"
msgstr "Pas de répétition"

#: inc/options-media-uploader.php:83
msgid "View File"
msgstr "Voir le fichier"

#: inc/options-media-uploader.php:59
msgid "Upgrade your version of WordPress for full media support."
msgstr "Upgrade your version of WordPress for full media support."

#: inc/options-media-uploader.php:56 inc/options-media-uploader.php:114
msgid "Remove"
msgstr "Retirer"

#: inc/options-media-uploader.php:54 inc/options-media-uploader.php:113
msgid "Upload"
msgstr "Envoyer"

#: inc/options-media-uploader.php:51
msgid "No file chosen"
msgstr "Aucun fichier choisi"

#: inc/options-framework.php:327
msgid "Options saved."
msgstr "Options enregistrées."

#: inc/options-framework.php:273
msgid "Default options restored."
msgstr "Les paramètres par défaut ont été restaurés."

#: inc/options-framework.php:241
msgid "Click OK to reset. Any theme settings will be lost!"
msgstr "Cliquez OK pour restaurer les paramètres par défaut. Tous les paramètres personnalisés seront perdus!"

#: inc/options-framework.php:241
msgid "Restore Defaults"
msgstr "Restaurer les paramètres par défaut"

#: inc/options-framework.php:240
msgid "Save Options"
msgstr "Sauvegarder les options"

#: inc/options-framework.php:195
msgid "Select Color"
msgstr "Sélectionner une couleur"

#: inc/options-framework.php:194
msgid "Default"
msgstr "Valeur par défaut"

#: inc/options-framework.php:148 inc/options-framework.php:377
msgid "Fifteen Settings"
msgstr "Fifteen Settings"

#: inc/options-framework.php:147
msgid "Fifteen Theme Options"
msgstr "Fifteen Theme Options"

#: inc/options-framework.php:193
msgid "Clear"
msgstr "Effacer"

#: inc/customizer.php:52
msgid "Primary Menu Color"
msgstr "Primary Menu Color"

#: inc/customizer.php:39
msgid "Site Description Color"
msgstr "Site Description Color"

#: inc/extras.php:66
msgid "Page %s"
msgstr "Page %s"

#: inc/customizer.php:26
msgid "Site Title Color"
msgstr "Couleur du titre du site"

#: image.php:38
msgid "Next <span class=\"meta-nav\">&rarr;</span>"
msgstr "Suivant <span class=\"meta-nav\">&rarr;</span>"

#: image.php:22
msgid "Published <span class=\"entry-date\"><time class=\"entry-date\" datetime=\"%1$s\">%2$s</time></span> at <a href=\"%3$s\">%4$s &times; %5$s</a> in <a href=\"%6$s\" rel=\"gallery\">%7$s</a>"
msgstr "Published <span class=\"entry-date\"><time class=\"entry-date\" datetime=\"%1$s\">%2$s</time></span> at <a href=\"%3$s\">%4$s &times; %5$s</a> in <a href=\"%6$s\" rel=\"gallery\">%7$s</a>"

#: image.php:37
msgid "<span class=\"meta-nav\">&larr;</span> Previous"
msgstr "<span class=\"meta-nav\">&larr;</span> Précédent"

#: header-single.php:67 header.php:66
msgid "Skip to content"
msgstr "Aller au contenu"

#: header-single.php:66 header.php:65
msgid "Menu"
msgstr "Menu"

#: functions.php:72
msgid "Footer Right"
msgstr "Footer Right"

#: functions.php:64
msgid "Footer Center"
msgstr "Footer Center"

#: functions.php:56
msgid "Footer Left"
msgstr "Footer Left"

#: functions.php:48
msgid "This is the Primary Sidebar. It will be displayed on Posts Pages."
msgstr "This is the Primary Sidebar. It will be displayed on Posts Pages."

#: functions.php:47
msgid "Primary Sidebar"
msgstr "Barre latérale principale"

#: functions.php:28
msgid "Top Menu"
msgstr "Menu Top"

#: footer.php:18
msgid "Fifteen Theme by %1$s."
msgstr "Fifteen Theme by %1$s."

#: functions.php:27
msgid "Primary Menu"
msgstr "Menu principal"

#: content-single.php:56
msgid "This entry was posted in %1$s. Bookmark the <a href=\"%3$s\" rel=\"bookmark\">permalink</a>."
msgstr "Cette entrée a été publiée dans %1$s . Marquer le <a href=\"%3$s\" rel=\"bookmark\"> permalien </a>."

#: content-single.php:54
msgid "This entry was posted in %1$s and tagged %2$s. Bookmark the <a href=\"%3$s\" rel=\"bookmark\">permalink</a>."
msgstr "Cette entrée a été publiée dans %1$s et étiqueté %2$s . Marquer le <a href=\"%3$s\" rel=\"bookmark\"> permalien </a>."

#: content-single.php:48
msgid "Bookmark the <a href=\"%3$s\" rel=\"bookmark\">permalink</a>."
msgstr "Marquer le <a href=\"%3$s\" rel=\"bookmark\">permalien</a>."

#: content-single.php:46
msgid "This entry was tagged %2$s. Bookmark the <a href=\"%3$s\" rel=\"bookmark\">permalink</a>."
msgstr "Cette entrée a été étiqueté %2$s . Marquer le <a href=\"%3$s\" rel=\"bookmark\"> permalien </a>."

#. translators: used between list items, there is a space after the comma
#: content-single.php:38 content-single.php:41
msgid ", "
msgstr ", "

#: content-search.php:45 content.php:39
msgid "Pages: "
msgstr "Pages&nbsp;:"

#: content-search.php:42 content.php:36
msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr "Continuer la lecture <span class=\"meta-nav\">&rarr;</span>"

#: content-page.php:20 content-single.php:69 image.php:32 image.php:64
#: inc/template-tags.php:71 inc/template-tags.php:89
msgid "Edit"
msgstr "Modifier"

#: content-page.php:15 content-single.php:29 image.php:58
msgid "Pages:"
msgstr "Pages&nbsp;:"

#: content-home.php:21
msgid "Read More"
msgstr "Lire la suite"

#: comments.php:102
msgid "Website"
msgstr "Site web"

#: comments.php:95
msgid "Email"
msgstr "Adresse de contact"

#: comments.php:89
msgid "Name"
msgstr "Nom"

#: comments.php:82
msgctxt "noun"
msgid "Comment"
msgstr "Commentaire"

#: comments.php:68
msgid "Comments are closed."
msgstr "Les commentaires sont fermés."

#: comments.php:38 comments.php:58
msgid "Newer Comments &rarr;"
msgstr "Commentaires plus récents &rarr;"

#: comments.php:37 comments.php:57
msgid "&larr; Older Comments"
msgstr "&larr; Commentaire plus ancien"

#: comments.php:36 comments.php:56
msgid "Comment navigation"
msgstr "Navigation des commentaires"

#: comments.php:29
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] "Une réflexion au sujet de &laquo;&nbsp;%2$s&nbsp;&raquo;"
msgstr[1] "%1$s réflexions au sujet de &laquo;&nbsp;%2$s&nbsp;&raquo;"

#: archive.php:56 sidebar.php:17
msgid "Archives"
msgstr "Archives"

#: archive.php:53
msgid "Links"
msgstr "Liens"

#: archive.php:50
msgid "Quotes"
msgstr "Citations"

#: archive.php:47
msgid "Videos"
msgstr "Vidéos"

#: archive.php:44
msgid "Images"
msgstr "Images"

#: archive.php:41
msgid "Asides"
msgstr "En passant"

#: archive.php:38
msgid "Year: %s"
msgstr "Année : %s"

#: archive.php:35
msgid "Month: %s"
msgstr "Mois : %s"

#: archive.php:32
msgid "Day: %s"
msgstr "Jour : %s"

#: archive.php:24
msgid "Author: %s"
msgstr "Auteur : %s"

#. translators: %1$s: smiley
#: 404.php:44
msgid "Try looking in the monthly archives. %1$s"
msgstr "Essayez de voir du côté des archives mensuelles. %1$s"

#: 404.php:27
msgid "Most Used Categories"
msgstr "Catégories les plus utilisées"

#: 404.php:19
msgid "It looks like nothing was found at this location. Maybe try one of the links below or a search?"
msgstr "Contenu Introuvable. L'outil de Recherche ou les Liens ci-dessous vous remettront peut-être sur la voie&hellip;"

#: 404.php:15
msgid "Oops! That page can&rsquo;t be found."
msgstr "Oups&nbsp;! Cette page est introuvable."
