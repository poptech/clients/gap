��          T      �       �   .  �      �     �  	   �       *     �   F  ^  4     �     �  	   �     �  *   �                                        A Storefront child theme designed for small WooCommerce stores / boutiques. Boutique features a simple, traditional design which you can customise using the settings available in the WordPress Customizer. Looking for a theme for your new WooCommerce store? Look no further than Storefront and Boutique! Boutique Homepage WooThemes http://woocommerce.com https://woocommerce.com/products/boutique/ PO-Revision-Date: 2016-09-13 13:29:03+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/2.1.0-alpha
Project-Id-Version: Boutique
 Un thème enfant de Storefront destiné aux petites boutiques WooCommerce. Les fonctionnalités de Boutique sont simples, avec un design traditionnel que vous pouvez personnaliser avec l’outil de personnalisation de WordPress. Vous cherchez un thème pour votre nouvelle boutique WooCommerce ? Ne cherchez pas plus loin que Storefront et Boutique ! Boutique Page d’accueil WooThemes http://woocommerce.com https://woocommerce.com/products/boutique/ 